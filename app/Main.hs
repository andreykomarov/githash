{-# LANGUAGE RecordWildCards #-}

module Main where

import Data.Git.Ref (cmpPrefix)
import Data.Monoid ((<>))
import Options.Applicative (Parser, execParser, flag, fullDesc, help, helper, info, long, metavar,
                            progDesc, short, strOption, (<**>))

import Lib

data Config = Config
    { cPrefix :: String
    , cUseGPU :: Bool
    }

config :: Parser Config
config = Config
    <$> strOption (short 'p'
                  <> long "prefix"
                  <> metavar "PREFIX"
                  <> help "Desired commit prefix in hex")
    <*> flag False True (short 'g'
                        <> long "gpu"
                        <> help "Whether to use CPU (default) or GPU."
                        )



main :: IO ()
main = run =<< execParser opts
  where
    opts = info (config <**> helper)
        ( fullDesc
        <> progDesc "Make your git commit hashes beautiful!"
        )

run :: Config -> IO ()
run Config{..} = do
    updateHead cUseGPU cPrefix >>= print
