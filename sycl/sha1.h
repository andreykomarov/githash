#ifndef OCL_SHA1_H
#define OCL_SHA1_H


class SHA1State {
    static constexpr size_t IT_PER_THREAD = 1000;
    static constexpr size_t BLOCKS = 32;
    static constexpr size_t THREADS = 64;

    static constexpr size_t BLOCK_SIZE = 64;
    static constexpr uint32_t H0_INIT = 0x67452301;
    static constexpr uint32_t H1_INIT = 0xEFCDAB89;
    static constexpr uint32_t H2_INIT = 0x98BADCFE;
    static constexpr uint32_t H3_INIT = 0x10325476;
    static constexpr uint32_t H4_INIT = 0xC3D2E1F0;
    static constexpr size_t DIGEST_SIZE = 20;

    uint32_t h0, h1, h2, h3, h4;
    uint8_t buf[BLOCK_SIZE];
    size_t buflen;
    uint64_t blocks;

    void sha1_update_with_block();
public:
    SHA1State();

    void update(const uint8_t *a, size_t len);
    void finalize(uint8_t* out);
    std::string finalize();
};

#endif //OCL_SHA1_H
