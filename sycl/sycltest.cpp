/* RUN: %{execute}%s | %{filecheck} %s
   CHECK: Result:
   CHECK-NEXT: 6 8 11
*/
#include <CL/sycl.hpp>
#include <iostream>
#include <iterator>
#include "sha1.h"

constexpr size_t N = 3;
using Vector = float[N];

int main() {
  Vector a = { 1, 2, 3 };
  Vector b = { 5, 6, 8 };
  Vector c;

  /*
  {
      cl::sycl::context myContext;
      std::vector<cl::sycl::buffer<int, 1>> bufferList {
          cl::sycl::buffer<int, 1>{},
          cl::sycl::buffer<int, 1>{cl::sycl::property::use_host_ptr{}},
          cl::sycl::buffer<int, 1>{cl::sycl::property::context_bound{myContext}}
      };
      for(auto& buf : bufferList) {
          if (buf.has_property<cl::sycl::property::context_bound>()) {
              auto prop = buf.get_property<property::context_bound>();
              assert(myContext == prop.get_context());
          }
      }
  }
   */

  // Create a queue to work on
  cl::sycl::queue q;

  // Create buffers from a & b vectors
  cl::sycl::buffer<float> A { std::begin(a), std::end(a) };
  cl::sycl::buffer<float> B { std::begin(b), std::end(b) };
  std::cout << "q: is_host=" << q.is_host() << std::endl;

    auto num_groups =
            q.get_device()
                    .get_info<cl::sycl::info::device::max_compute_units>();
    // getting the maximum work group size per thread
    auto work_group_size =
            q.get_device()
                    .get_info<cl::sycl::info::device::max_work_group_size>();

    std::cout << "Num groups = " << num_groups << ", group size = " << work_group_size << std::endl;
    // building the best number of global thread

  {
    // A buffer of N float using the storage of c
    cl::sycl::buffer<float> C { c, N };

    /* The command group describing all operations needed for the kernel
       execution */
    auto e = q.submit([&](cl::sycl::handler &cgh) {
        // In the kernel A and B are read, but C is written
        auto ka = A.get_access<cl::sycl::access::mode::read>(cgh);
        auto kb = B.get_access<cl::sycl::access::mode::read>(cgh);
        auto kc = C.get_access<cl::sycl::access::mode::write>(cgh);

        // Enqueue a parallel kernel
        cgh.parallel_for<class vector_add>(N,
                                           [=] (cl::sycl::id<1> index) {
                                             kc[index] = ka[index] + kb[index];
                                             std::cout << "Lol from kernel" << std::endl;
                                               SHA1State s;
                                               std::cout << "sha1('') = " << s.finalize() << std::endl;
                                           });
//        cgh.parallel_for_work_group<class lolka>(cl::sycl::nd_range({3}, {1}), [=](cl::sycl::nd_item<1> index) {
//            std::cout << "idx = " << index.get_global_id().get(0) << std::endl;
//            kc[index] = ka[index] + kb[index];
//        });
    }); //< End of our commands for this queue
  } //< Buffer C goes out of scope and copies back values to c


  std::cout << std::endl << "Result:" << std::endl;
  for (auto e : c)
    std::cout << e << " ";
  std::cout << std::endl;

  SHA1State s;
  std::cout << "sha1('') = " << s.finalize() << std::endl;
}
