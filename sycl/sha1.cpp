#include <cstdint>
#include <cstddef>
#include <cstring>
#include <array>
#include "sha1.h"

namespace {
    void put_be64(uint8_t *a, uint64_t val) {
        // TODO check if actually BE
        a += 8;
        for (int i = 0; i < 8; i++) {
            a--;
            *a = val & 0xff;
            val >>= 8;
        }
    }

    void put_be32(uint8_t *a, uint32_t val) {
        // TODO check if actually BE
        a += 4;
        for (int i = 0; i < 4; i++) {
            a--;
            *a = val & 0xff;
            val >>= 8;
        }
    }

    uint32_t get_be32(const uint8_t *a) {
        uint32_t res = 0;
        for (int i = 0; i < 4; i++)
        {
            res <<= 8;
            res |= a[i];
        }
        return res;
    }

    uint32_t rol(uint32_t x, uint32_t shift) {
        return (x << shift) | (x >> (32 - shift));
    }
}

SHA1State::SHA1State(): h0(H0_INIT), h1(H1_INIT), h2(H2_INIT), h3(H3_INIT), h4(H4_INIT), buflen(0), blocks(0){
}

void SHA1State::update(const uint8_t *a, size_t len) {
    while (len + buflen >= BLOCK_SIZE) {
        size_t here = BLOCK_SIZE - buflen;
        memcpy(buf + buflen, a, here);
        buflen += here;
        sha1_update_with_block();
        a += here;
        len -= here;
    }
    memcpy(buf + buflen, a, len);
    buflen += len;
}

void SHA1State::sha1_update_with_block() {
    uint32_t w[80];
    for (int i = 0; i < 16; i++)
        w[i] = get_be32(buf + 4 * i);
    for (int i = 16; i < 80; i++)
        w[i] = rol(w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16], 1);

    uint32_t a = h0;
    uint32_t b = h1;
    uint32_t c = h2;
    uint32_t d = h3;
    uint32_t e = h4;

    for (int i = 0; i < 80; i++)
    {
        uint32_t f, k;
        if (i < 20)
            f = (b & c) | ((~b) & d), k = 0x5A827999;
        else if (i < 40)
            f = b ^ c ^ d, k = 0x6ED9EBA1;
        else if (i < 60)
            f = (b & c) | (b & d) | (c & d), k = 0x8F1BBCDC;
        else
            f = b ^ c ^ d, k = 0xCA62C1D6;

        uint32_t tmp = rol(a, 5) + f + e + k + w[i];
        e = d;
        d = c;
        c = rol(b, 30);
        b = a;
        a = tmp;
    }

    h0 += a;
    h1 += b;
    h2 += c;
    h3 += d;
    h4 += e;
    buflen = 0;
    blocks++;
}

void SHA1State::finalize(uint8_t *out) {
    uint64_t byte_len = blocks * BLOCK_SIZE + buflen;
    uint64_t bit_len = 8 * byte_len;
    uint8_t a[] = {0x80};
    update(a, 1);
    memset(buf + buflen, 0, BLOCK_SIZE - buflen);
    if (buflen < BLOCK_SIZE - 8) {
        put_be64(buf + BLOCK_SIZE - 8, bit_len);
        buflen = BLOCK_SIZE;
        sha1_update_with_block();
    } else {
        sha1_update_with_block();
        memset(buf, 0, BLOCK_SIZE);
        put_be64(buf + BLOCK_SIZE - 8, bit_len);
        buflen = BLOCK_SIZE;
        sha1_update_with_block();
    }

    put_be32(out, h0);
    put_be32(out + 4, h1);
    put_be32(out + 8, h2);
    put_be32(out + 12, h3);
    put_be32(out + 16, h4);
}

std::string SHA1State::finalize() {
    constexpr char HEX[] = "0123456789abcdef";
    uint8_t out[DIGEST_SIZE];
    finalize(out);
    std::string res;
    for (size_t i = 0; i < DIGEST_SIZE; i++) {
        res.push_back(HEX[out[i] >> 4]);
        res.push_back(HEX[out[i] & 0xf]);
    }
    return res;
}
