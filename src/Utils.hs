-- |

module Utils
       ( calcRef
       ) where

import Crypto.Hash (HashAlgorithm)
import Data.Git (Commit (commitExtras), CommitExtra (..), Ref, setObject)
import Data.Git.Storage.Object (Object, Objectable (getRaw, getType, toObject), objectHash)

import qualified Data.ByteString.Lazy as BSL

calcRef :: (HashAlgorithm hash ,Objectable o)
        => o hash -> Ref hash
calcRef obj = objectHash (getType obj) (fromIntegral $ BSL.length bs) bs
  where
    bs = getRaw obj

