-- |

module UpdateCommitExt
       ( updateCommitGPU
       ) where

import Crypto.Hash (HashAlgorithm)
import Data.ByteString (ByteString, breakSubstring, hPut)
import Data.ByteString.Char8 (pack)
import Data.Git (Commit (commitExtras), CommitExtra (..), Ref, setObject)
import Data.Git.Monad (GitMonad (getGit, liftGit), getCommit, headResolv, withCurrentRepo)
import Data.Git.Ref (SHA1)
import Data.Git.Storage.Object (Object, Objectable (getRaw, getType, toObject), objectHash,
                                objectWriteHeader)
import Data.Monoid ((<>))
import System.IO (hClose)
import System.IO.Temp (withSystemTempFile)
import System.Process (readProcess)


import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL

secret :: ByteString
secret = pack $ "wOwSOsEcR3t10101"

patchSig :: HashAlgorithm hash => ByteString -> Commit hash -> Commit hash
patchSig num commit = case commitExtras commit of
    [CommitExtra key@"gpgsig -----BEGIN PGP SIGNATURE-----" val] -> commit
        { commitExtras = [CommitExtra key ("Comment: " <> (pack $ show num) <> "\n" <> val)]
        }
    _                                                        -> error "not implemented"

patch :: HashAlgorithm hash => ByteString -> Commit hash -> Commit hash
patch adj commit = if null (commitExtras commit)
    then commit
         { commitExtras = [CommitExtra "adjusthash" adj]
         }
    else patchSig adj commit

updateCommitGPU :: String -> Commit SHA1 -> IO (Commit SHA1)
updateCommitGPU prefix commit = do
    withSystemTempFile "hashgit-pref" $ \fnPref hPref ->
     withSystemTempFile "hashgit-suf" $ \fnSuf hSuf -> do
        let (pref, suf') = breakSubstring secret $ BSL.toStrict bytes
        let suf = BS.drop (BS.length secret) suf'
        hPut hPref pref >> hClose hPref
        hPut hSuf suf >> hClose hSuf
        out <- head <$> lines <$> readProcess "sha1" [fnPref, show (BS.length secret), fnSuf, prefix] ""
        print out
        pure commit
              { commitExtras =
                      CommitExtra "adjusthash" (pack out)
                      :commitExtras commit
              }
  where
    commit' = patch secret commit

    raw = getRaw commit'
    bytes = BSL.fromChunks $ objectWriteHeader (getType commit') (fromIntegral $ BSL.length raw) : BSL.toChunks raw

