module Lib
    ( updateHead
    ) where

import Control.Concurrent (getNumCapabilities)
import Control.Concurrent.Async (async, waitAny)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TBQueue (TBQueue, newTBQueueIO, readTBQueue, writeTBQueue)
import Control.Monad (replicateM)
import Crypto.Hash (HashAlgorithm)
import Data.ByteString.Char8 (pack)
import Data.Git (Commit (commitExtras), CommitExtra (..), Ref, setObject)
import Data.Git.Monad (GitMonad (getGit, liftGit), getCommit, headResolv, withCurrentRepo)
import Data.Git.Ref (SHA1)
import Data.Git.Storage.Object (Object, Objectable (getRaw, getType, toObject), objectHash)

import UpdateCommitCPU (updateCommit)
import UpdateCommitExt (updateCommitGPU)
import Utils (calcRef)

import qualified Data.ByteString.Lazy as BSL

updateHead :: Bool -> String -> IO (Either String ())
updateHead useGPU prefix = withCurrentRepo $ do
    ref <- headResolv >>= unmaybe "head"
    commit <- getCommit ref >>= unmaybe "commit"
    let update = if useGPU then updateCommitGPU else updateCommit
    newCommit <- liftGit $ update prefix commit
    liftGit $ print newCommit
    liftGit $ print $ calcRef newCommit
    git <- getGit
    liftGit $ setObject git $ toObject newCommit
    pure ()

unmaybe :: Monad m => String -> Maybe a -> m a
unmaybe msg ma = case ma of
    Nothing -> fail msg
    Just a  -> pure a
