-- |

module UpdateCommitCPU
       ( updateCommit
       ) where

import Control.Concurrent (getNumCapabilities)
import Control.Concurrent.Async (async, waitAny)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TBQueue (TBQueue, newTBQueueIO, readTBQueue, writeTBQueue)
import Control.Monad (replicateM)
import Crypto.Hash (HashAlgorithm)
import Data.ByteString.Char8 (pack)
import Data.Git (Commit (commitExtras), CommitExtra (..), Ref, setObject)
import Data.Git.Monad (GitMonad (getGit, liftGit), getCommit, headResolv, withCurrentRepo)
import Data.Git.Ref (SHA1, cmpPrefix)
import Data.Git.Storage.Object (Object, Objectable (getRaw, getType, toObject), objectHash)
import Data.Monoid ((<>))

import Utils (calcRef)

import qualified Data.ByteString.Lazy as BSL

patchSig :: HashAlgorithm hash => Int -> Commit hash -> Commit hash
patchSig num commit = case commitExtras commit of
    [CommitExtra key@"gpgsig -----BEGIN PGP SIGNATURE-----" val] -> commit
        { commitExtras = [CommitExtra key ("Comment: " <> (pack $ show num) <> "\n" <> val)]
        }
    _                                                        -> error "not implemented"

patch :: HashAlgorithm hash => Int -> Commit hash -> Commit hash
patch num commit = if null (commitExtras commit)
    then commit
         { commitExtras = [CommitExtra "adjusthash" (pack $ show num)]
         }
    else patchSig num commit

updateCommit :: String -> Commit SHA1 -> IO (Commit SHA1)
updateCommit prefix commit = do
    procs <- getNumCapabilities
    print procs
    tasks <- newTBQueueIO procs
    threads <- replicateM procs (async $ worker tasks)
    async $ sender tasks 100 0
    (_, res) <- waitAny threads
    pure res
  where
    pred :: Ref SHA1 -> Bool
    pred = ((== EQ) . cmpPrefix prefix)
    sender :: TBQueue (Int, Int) -> Int -> Int -> IO ()
    sender q step lo = do
        atomically $ writeTBQueue q (lo, lo + step)
        sender q step (lo + step)

    worker :: TBQueue (Int, Int) -> IO (Commit SHA1)
    worker q = do
        (lo, hi) <- atomically $ readTBQueue q
        res <- loop lo hi
        case res of
            Nothing  -> worker q
            Just res -> pure res
      where
        loop lo hi = do
            if lo < hi
            then do
                let c' = patch lo commit
                if pred (calcRef c') then pure (Just c') else loop (lo + 1) hi
            else pure Nothing
