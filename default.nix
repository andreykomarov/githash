let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
in rec {
  githash = stdenv.mkDerivation rec {
    name = "githash";
    src = ./.;
    buildInputs = [ pkgs.cudatoolkit pkgs.linuxPackages.nvidia_x11 pkgs.makeWrapper ];

    preBuild = ''
#      export CUDA_PATH="${pkgs.cudatoolkit}"
    '';

    installPhase = ''
      mkdir -p "$out/"
      #cp vectorAdd "$out/vectorAdd"
      cp cuda/sha1 "$out/sha1"
      #cp "$(stack path --local-install-root)/bin/githash" "$out/githash"
    '';


  };
}
