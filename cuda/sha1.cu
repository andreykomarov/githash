#include <cuda_runtime.h>
#include <cstdint>
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

constexpr size_t IT_PER_THREAD = 1000;
constexpr size_t BLOCKS = 32;
constexpr size_t THREADS = 64;

constexpr size_t BLOCK_SIZE = 64;
constexpr uint32_t h0_init = 0x67452301;
constexpr uint32_t h1_init = 0xEFCDAB89;
constexpr uint32_t h2_init = 0x98BADCFE;
constexpr uint32_t h3_init = 0x10325476;
constexpr uint32_t h4_init = 0xC3D2E1F0;

struct State {
  uint32_t h0, h1, h2, h3, h4;
  uint8_t buf[BLOCK_SIZE];
  size_t buflen;
  uint64_t blocks;
};

__device__ void
put_be64(uint8_t *a, uint64_t val)
{
  // TODO check if actually BE
  a += 8;
  for (int i = 0; i < 8; i++)
    {
      a--;
      *a = val & 0xff;
      val >>= 8;
    }
}

__device__ void
put_be32(uint8_t *a, uint32_t val)
{
  // TODO check if actually BE
  a += 4;
  for (int i = 0; i < 4; i++)
    {
      a--;
      *a = val & 0xff;
      val >>= 8;
    }
}

__device__ uint32_t
get_be32(const uint8_t *a)
{
  uint32_t res = 0;
  for (int i = 0; i < 4; i++)
    {
      res <<= 8;
      res |= a[i];
    }
  return res;
}

__device__ uint32_t rol(uint32_t x, uint32_t shift)
{
  return (x << shift) | (x >> (32 - shift));
}

__device__ void
sha1_update_with_block(State *s)
{
  uint32_t w[80];
  for (int i = 0; i < 16; i++)
    w[i] = get_be32(s->buf + 4 * i);
  for (int i = 16; i < 80; i++)
    w[i] = rol(w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16], 1);

  uint32_t a = s->h0;
  uint32_t b = s->h1;
  uint32_t c = s->h2;
  uint32_t d = s->h3;
  uint32_t e = s->h4;

  for (int i = 0; i < 80; i++)
    {
      uint32_t f, k;
      if (i < 20)
        f = (b & c) | ((~b) & d), k = 0x5A827999;
      else if (i < 40)
        f = b ^ c ^ d, k = 0x6ED9EBA1;
      else if (i < 60)
        f = (b & c) | (b & d) | (c & d), k = 0x8F1BBCDC;
      else
        f = b ^ c ^ d, k = 0xCA62C1D6;

      uint32_t tmp = rol(a, 5) + f + e + k + w[i];
      e = d;
      d = c;
      c = rol(b, 30);
      b = a;
      a = tmp;
    }

  s->h0 += a;
  s->h1 += b;
  s->h2 += c;
  s->h3 += d;
  s->h4 += e;
  s->buflen = 0;
  s->blocks++;
}

__device__ void
sha1_update(State* s, const uint8_t *a, size_t len)
{
  while (len + s->buflen >= BLOCK_SIZE)
    {
      size_t here = BLOCK_SIZE - s->buflen;
      memcpy(s->buf + s->buflen, a, here);
      s->buflen += here;
      sha1_update_with_block(s);
      a += here;
      len -= here;
    }
  memcpy(s->buf + s->buflen, a, len);
  s->buflen += len;
}

__device__ void
sha1_finalize(State* s, uint8_t* out)
{
  uint64_t byte_len = s->blocks * BLOCK_SIZE + s->buflen;
  uint64_t bit_len = 8 * byte_len;
  sha1_update(s, "\x80", 1);
  memset(s->buf + s->buflen, 0, BLOCK_SIZE - s->buflen);
  if (s->buflen < BLOCK_SIZE - 8)
    {
      put_be64(s->buf + BLOCK_SIZE - 8, bit_len);
      s->buflen = BLOCK_SIZE;
      sha1_update_with_block(s);
    }
  else
    {
      sha1_update_with_block(s);
      memset(s->buf, 0, BLOCK_SIZE);
      put_be64(s->buf + BLOCK_SIZE - 8, bit_len);
      s->buflen = BLOCK_SIZE;
      sha1_update_with_block(s);
    }

  put_be32(out, s->h0);
  put_be32(out + 4, s->h1);
  put_be32(out + 8, s->h2);
  put_be32(out + 12, s->h3);
  put_be32(out + 16, s->h4);
}

__device__ void
sha1_init(State* s)
{
  s->h0 = h0_init;
  s->h1 = h1_init;
  s->h2 = h2_init;
  s->h3 = h3_init;
  s->h4 = h4_init;
  s->buflen = 0;
  s->blocks = 0;
}

__global__ void
sha1(const uint8_t *a, size_t len, uint8_t *out)
{
  State s;
  sha1_init(&s);
  sha1_update(&s, a, len);
  sha1_finalize(&s, out);
}

__global__ void
look_for_prefix(const uint8_t *begin, size_t begin_len,
                const uint8_t *end, size_t end_len,
                size_t add,
                const char *prefix, size_t prefix_len,
                const uint64_t *seeds,
                uint8_t *result)
{
  constexpr char HEX[] = "0123456789abcdef";

  size_t idx = blockDim.x * blockIdx.x + threadIdx.x;
  uint64_t seed = seeds[idx];

  State s;
  sha1_init(&s);
  sha1_update(&s, begin, begin_len);

  uint8_t *end2 = new uint8_t[end_len];
  memcpy(end2, end, end_len);

  char *prefix2 = new char[prefix_len];
  memcpy(prefix2, prefix, prefix_len);

  uint8_t *buf = new uint8_t[add + 1];
  for (int it = 0; it < IT_PER_THREAD; it++)
    {
      buf[add] = 0;
      uint64_t here = seed++;
      for (int i = 0; i < add; i++)
        {
          buf[i] = 'a' + (here & 0xf);
          here >>= 4;
        }
      State s2;
      memcpy(&s2, &s, sizeof(s2));
      sha1_update(&s2, buf, add);
      sha1_update(&s2, end2, end_len);
      uint8_t out[20];
      sha1_finalize(&s2, out);
      char out_hex[41];
      out_hex[40] = 0;
      for (int i = 0; i < 20; i++)
        {
          out_hex[2 * i] = HEX[out[i] >> 4];
          out_hex[2 * i + 1] = HEX[out[i] & 0xf];
        }
      bool good = true;
      for (int i = 0; i < prefix_len; i++)
        good &= out_hex[i] == prefix2[i];
      if (good)
        {
          //printf("Win! <%s> gives %s\n", buf, out_hex);
          memcpy(result, buf, add);
        }
    }
  delete[] end2;
  delete[] prefix2;
  delete[] buf;
}

void look_for_prefix_h(const string begin, const string end, size_t add, const char *prefix)
{
  size_t prefix_len = strlen(prefix);

  // Error code to check return values for CUDA calls
  cudaError_t err = cudaSuccess;
    
  uint8_t *d_begin = NULL;
  err = cudaMalloc((uint8_t**)&d_begin, begin.size());
  if (err != cudaSuccess)
    {
      cerr << "cudaMalloc failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  uint8_t *d_end = NULL;
  err = cudaMalloc((uint8_t**)&d_end, end.size());
  if (err != cudaSuccess)
    {
      cerr << "cudaMalloc failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  char *d_prefix = NULL;
  err = cudaMalloc((uint8_t**)&d_prefix, prefix_len);
  if (err != cudaSuccess)
    {
      cerr << "cudaMalloc failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  err = cudaMemcpy(d_begin, begin.c_str(), begin.size(), cudaMemcpyHostToDevice);
  if (err != cudaSuccess)
    {
      cerr << "cudaMemcpy failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  uint8_t *d_result = NULL;
  err = cudaMalloc((uint8_t**)&d_result, add);
  if (err != cudaSuccess)
    {
      cerr << "cudaMalloc failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  err = cudaMemcpy(d_end, end.c_str(), end.size(), cudaMemcpyHostToDevice);
  if (err != cudaSuccess)
    {
      cerr << "cudaMemcpy failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  err = cudaMemcpy(d_prefix, prefix, prefix_len, cudaMemcpyHostToDevice);
  if (err != cudaSuccess)
    {
      cerr << "cudaMemcpy failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  size_t seeds_size = BLOCKS * THREADS * sizeof(uint64_t);

  uint64_t *h_seeds = (uint64_t*)malloc(seeds_size);

  uint64_t *d_seeds = NULL;
  err = cudaMalloc((uint8_t**)&d_seeds, seeds_size);
  if (err != cudaSuccess)
    {
      cerr << "cudaMalloc failed: " << cudaGetErrorString(err) << "\n";
      exit(EXIT_FAILURE);
    }

  uint64_t expected = 1;
  for (int i = 0; i < prefix_len; i++)
    expected *= 16;

  uint64_t completed = 0;

  clock_t tick = clock();

  while (true)
    {
      for (int i = 0; i < BLOCKS * THREADS; i++)
        {
          for (int j = 0; j < 4; j++)
            {
              h_seeds[i] ^= rand();
              h_seeds[i] <<= 16;
            }
        }
    
      err = cudaMemcpy(d_seeds, h_seeds, seeds_size, cudaMemcpyHostToDevice);
      if (err != cudaSuccess)
        {
          cerr << "cudaMemcpy (seeds) failed: " << cudaGetErrorString(err) << "\n";
          exit(EXIT_FAILURE);
        }
    
      double mhs = 1. * completed / ((clock() - tick) / CLOCKS_PER_SEC) / 1000000;
      cerr << "Completed: " << completed << "/" << expected << " (" << mhs << " MH/s)\n";
      completed += BLOCKS * THREADS * IT_PER_THREAD;

      look_for_prefix<<<BLOCKS, THREADS>>>(d_begin, begin.size(), d_end, end.size(), add, d_prefix, prefix_len, d_seeds, d_result);
      err = cudaGetLastError();
      if (err != cudaSuccess)
        {
          cerr << "kernel launch failed: " << cudaGetErrorString(err) << "\n";
          exit(EXIT_FAILURE);
        }

      uint8_t h_result[add];
      err = cudaMemcpy(h_result, d_result, add, cudaMemcpyDeviceToHost);
      if (err != cudaSuccess)
        {
          cerr << "cudaMemcpy failed: " << cudaGetErrorString(err) << "\n";
          exit(EXIT_FAILURE);
        }
      for (int i = 0; i < add; i++)
        if (h_result[i] != 0)
          {
            cerr << "Win!\n";
            for (int j = 0; j < add; j++)
              cout << (char)h_result[j];
            cout << "\n";
            exit(EXIT_SUCCESS);
          }
    }
}

string read_file(char *fname)
{
  ifstream in(fname);
  stringstream buf;
  buf << in.rdbuf();
  return buf.str();
}

int main(int argc, char **argv)
{
  if (argc != 5)
    {
      cerr << "Usage: " << argv[0] << " <prefix-file> <additional length> <suffix-file> <desired sha1 prefix>\n";
      exit(EXIT_FAILURE);
    }
  srand(time(NULL));
  string begin = read_file(argv[1]);
  string end = read_file(argv[3]);
  size_t add = atoi(argv[2]);
  char *prefix = argv[4];
  //  look_for_prefix_h("loool_", "_wtf", 16, "123456789");
  look_for_prefix_h(begin, end, add, prefix);
}