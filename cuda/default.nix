let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
in rec {
  sha1pref = stdenv.mkDerivation rec {
    name = "sha1pref";
    src = ./.;
    buildInputs = [ pkgs.clang pkgs.cudatoolkit pkgs.linuxPackages.nvidia_x11 pkgs.makeWrapper ];

    preBuild = ''
#      export CUDA_PATH="${pkgs.cudatoolkit}"
    '';

    installPhase = ''
      mkdir -p "$out/"
      #cp vectorAdd "$out/vectorAdd"
      cp sha1 "$out/sha1"
    '';


  };
}
